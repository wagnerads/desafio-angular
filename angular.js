var app = angular.module("app", []);

app.controller("Ctrl1", function($scope, GetPosts, $interval){
	GetPosts.get(function(posts){
		$scope.posts = posts.data.children;
	});
	$scope.ordem = "data.title";
	$scope.reverse = false;
	$scope.favorites = [];
	$scope.limit = 10;
	$scope.update = 0;

	var interval;

	$scope.change_interval = function() {
		if($scope.update == 0) {
			$interval.cancel(interval);
		}else{
			$interval.cancel(interval);
			interval = $interval(function() {
				GetPosts.get(function(posts){
					$scope.posts = posts.data.children;
				});
		     }, ($scope.update*1000));
		}
	}

	$scope.change_reverse = function() {
		if($scope.ordem == "data.title") {
			$scope.reverse = false;
		} else {
			$scope.reverse = true;
		}
	}

	$scope.change_limit = function(limit) {
		$scope.limit = limit;
	}
});

app.factory("GetPosts", function($http){
	return {
		get: function(callback){
			return $http.get("http://www.reddit.com/.json").success(callback);
		}
	}
});

app.filter('truncate', function () {
        return function (text, length, end) {
            if (isNaN(length))
                length = 10;

            if (end === undefined)
                end = "...";

            if (text.length <= length || text.length - end.length <= length) {
                return text;
            }
            else {
                return String(text).substring(0, 30-end.length) + "...";
            }

        };
    });

app.directive("view", function(){
	return {
		restrict: "E",	
		scope: {
			post: "=",
			favorite: "="
		},
		// templateUrl: 'view.html'
		template: '<div class="col-sm-6 col-md-3"><div class="thumbnail" style="min-height: 300px;"><img src="{{post.data.thumbnail}}" style="width: 100px; height: 100px;"><div class="caption"><h4>{{post.data.title|truncate:30}}</h4><p>Ups: {{post.data.ups}}</p><p>Downs: {{post.data.downs}}</p><p><a href="#" ng-click="favoritar()" ng-hide="favorite" class="btn btn-primary">Favoritar</a><a href="#" ng-show="favorite" ng-click="desfavoritar()" class="btn btn-danger">Desfavoritar</a></p></div></div></div>',
		link: function(scope, element, attr) {
			scope.favoritar = function() {
				scope.$parent.favorites.push(scope.post);
				scope.$parent.posts.splice( scope.$parent.posts.indexOf(scope.post), 1 );
			},
			scope.desfavoritar = function() {
				scope.$parent.posts.push(scope.post);
				scope.$parent.favorites.splice( scope.$parent.favorites.indexOf(scope.post), 1 );	
			}
		}
	}
});